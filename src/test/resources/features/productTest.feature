Feature: Product Test web service REST

  Scenario: get product by the given identifier
    When the client requests GET /api/commands/getProductById/1
    Then the JSON response should include "product1"
    And the JSON response should  include "productOne"
