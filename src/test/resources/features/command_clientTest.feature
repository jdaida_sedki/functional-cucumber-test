Feature: Command Test web service REST

  Scenario: get commands by the given client identifier
    When the client requests GET /api/commands/getCommandsByClient/1
    Then the response is json array witch have size equals to "2" 
    And the JSON response should include attribute name witch have value equals to
    |command02| 
    |command04|
    And the JSON response contain number value equals 
    |10|
    |11|


  Scenario: get commands by the given client identifier and command identifier
    When the client requests GET /api/commands/getCommandById/1/2
    Then the JSON response should contains attribute value equals to "command04"
    And the JSON response should  include number value equals to "11"
