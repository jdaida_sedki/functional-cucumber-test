package com.dynamix.test.stepdefs;

import static org.junit.Assert.assertEquals;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;

import com.dynamix.test.model.Product;
import com.dynamix.test.ws.RetrieveUtil;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * 
 * @author fatma
 *
 */
public class ProductFeatureDef {

	HttpResponse httpResponse = null;
	
	Product productModel;
   
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@When("^the client requests GET /api/commands/getProductById/(\\d+)$")
	public void the_client_requests_GET_api_commands_getProductById(int arg1) throws Throwable {
		HttpUriRequest request = new HttpGet("http://localhost:8080/api/commands/getProductById/" + arg1);
		httpResponse = HttpClientBuilder.create().build().execute(request);
		productModel = RetrieveUtil.retrieveResourceFromResponse(httpResponse, Product.class);
	}
    
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the JSON response should include \"(.*?)\"$")
	public void the_JSON_response_should_include(String arg1) throws Throwable {
		assertEquals(productModel.getCode(), arg1);
	}
    
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the JSON response should not include \"(.*?)\"$")
	public void the_JSON_response_should_not_include(String arg1) throws Throwable {
		assertEquals(productModel.getLabel().toString(), arg1);
	}

}
