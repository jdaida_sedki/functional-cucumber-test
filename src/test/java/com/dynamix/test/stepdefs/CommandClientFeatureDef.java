/**
 * 
 */
package com.dynamix.test.stepdefs;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;

import com.dynamix.test.model.Command;
import com.dynamix.test.ws.RetrieveUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Jdaida SEDKI
 *
 */
public class CommandClientFeatureDef {
    
	/**
	 * variable for httpResponse to recuperate the result of service web response
	 */
	HttpResponse httpResponse = null;
	
	/**
	 * object variable of command
	 */
	Command command;
    
	/**
	 * list of commands 
	 */
	List<Command> commands;
	
	
     /**
      * send a request with client identifier to recuperate the list of her commands
      * @param arg1
      * @throws Throwable
      */
	@When("^the client requests GET /api/commands/getCommandsByClient/(\\d+)$")
	public void the_client_requests_GET_api_commands_getCommandsByProfile(int arg1) throws Throwable {
		HttpUriRequest request = new HttpGet("http://localhost:8080/api/commands/getCommandsByClient/" + arg1);
		httpResponse = HttpClientBuilder.create().build().execute(request);
		HttpEntity entity = httpResponse.getEntity();
		String result = EntityUtils.toString(entity);
		ObjectMapper mapper = new ObjectMapper();
		commands = mapper.readValue(result, new TypeReference<List<Command>>() {
		});
	}
    
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the response is json array witch have size equals to \"(.*?)\"$")
	public void the_response_is_json_array_witch_have_size_equals_to(String arg1) throws Throwable {
		Assert.assertEquals(commands.size(), Integer.parseInt(arg1));
	}
    
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the JSON response contain number value equals$")
	public void the_JSON_response_contain_number_value_equals(List<Integer> arg1) throws Throwable {
		List<Integer> commandsNumber = commands.stream().map(Command::getNumber).collect(Collectors.toList());
		commandsNumber.forEach(System.out::println);
		Assert.assertEquals(commandsNumber, arg1);
	}
    
	/**
	 * 
	 * @param arg1
	 * @param arg2
	 * @throws Throwable
	 */
	@When("^the client requests GET /api/commands/getCommandById/(\\d+)/(\\d+)$")
	public void the_client_requests_GET_api_commands_getCommandById(int arg1, int arg2) throws Throwable {
		HttpUriRequest request = new HttpGet("http://localhost:8080/api/commands/getCommandById/" + arg1 + "/" + arg2);
		httpResponse = HttpClientBuilder.create().build().execute(request);
		command = RetrieveUtil.retrieveResourceFromResponse(httpResponse, Command.class);
	}
    
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the JSON response should contains attribute value equals to \"(.*?)\"$")
	public void the_JSON_response_should_contains_attribute_value_equals_to(String arg1) throws Throwable {
		assertEquals(command.getName(), arg1);
	}
   
	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the JSON response should include attribute name witch have value equals to$")
	public void the_JSON_response_should_contains_attribute_value_equals_to_values(List<String> arg1) throws Throwable {
		List<String> commandNames = commands.stream().map(Command::getName).collect(Collectors.toList());
		commandNames.forEach(System.out::println);
		Assert.assertEquals(commandNames, arg1);

	}

	/**
	 * 
	 * @param arg1
	 * @throws Throwable
	 */
	@Then("^the JSON response should  include number value equals to \"(.*?)\"$")
	public void the_JSON_response_should_include_number_value_equals_to(String arg1) throws Throwable {
		assertEquals(command.getNumber(), Integer.parseInt(arg1));
	}

}
