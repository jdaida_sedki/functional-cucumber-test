package com.dynamix.test.ws;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RetrieveUtil {

	// API

	public static <T> T retrieveResourceFromResponse(final HttpResponse response, final Class<T> clazz)
			throws IOException {
		final String jsonFromResponse = EntityUtils.toString(response.getEntity());
		final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
				false);
		return mapper.readValue(jsonFromResponse, clazz);
	}

	public static <T> List<T> parseJsonArray(String json, Class<T> classOnWhichArrayIsDefined)
			throws IOException, ClassNotFoundException {
		ObjectMapper mapper = new ObjectMapper();
		Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + classOnWhichArrayIsDefined.getName() + ";");
		T[] objects = mapper.readValue(json, arrayClass);
		return Arrays.asList(objects);
	}

}
