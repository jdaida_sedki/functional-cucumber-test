/**
 * 
 */
package com.dynamix.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

/**
 * @author Jdaida SEDKI
 *
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "html:reports/taskManager/ReportFlowcucumber-html-report", "junit:reports/taskManager/ReportFlowcucumber-junit.xml",
		"json:reports/taskManager/ReportFlowcucumber.json", "pretty:reports/taskManager/ReportFlowcucumber-pretty.txt",
		"usage:reports/taskManager/ReportFlowcucumber-usage.json" }, features = {
				"src/test/resources/features/command_clientTest.feature" }, glue = {
						"com.sagemcom.test.stepdefs" }, tags = { "~@Skip" })
public class RunnerCommandClient {

}
